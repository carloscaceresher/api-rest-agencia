'use strict'

const port = process.env.PORT || 4000;
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

const URL_RESERVAS = 'https://localhost:3900/';
const URL_USUARIOS = 'https://localhost:4100/';

const https = require('https');
const fs = require('fs');
const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const Token = require('./services/token.service');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');

function auth(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
        return next(new Error("falta token de autorizacion"));
    }
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            return next();
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
            return next(err);
        });
}

const app = express();
//declaramos Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.post('/registro',(req,res,next)=>{
    const queURL = `${URL_USUARIOS}signup`;
    fetch(queURL,{
        method:'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }

    })
    .then( resp => resp.json() )
    .then( respuesta => {
        res.json({
            respuesta
        });
    });
});

app.post('/login',(req,res,next)=>{
    const queURL = `${URL_USUARIOS}login`;
    fetch(queURL,{
        method:'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }

    })
    .then( resp => resp.json() )
    .then( respuesta => {
        res.json({
            respuesta
        });
    });
});

app.get('/listaVehiculos',auth,(req,res,next)=>{
    const queURL = `${URL_RESERVAS}listaVehiculos`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( list => {
        res.json({
            list
        });
    });
});

app.get('/listaVuelos',auth,(req,res,next)=>{
    const queURL = `${URL_RESERVAS}listaVuelos`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( list => {
        res.json({
            list
        });
    });
});

app.get('/listaHoteles',auth,(req,res,next)=>{
    const queURL = `${URL_RESERVAS}listaHoteles`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( list => {
        res.json({
            list
        });
    });
});

app.post('/reservaVuelo',(req,res,next)=>{
    const queURL = `${URL_RESERVAS}reservaVuelo`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( respuesta => {
        res.json({
            respuesta
        });
    });
});

app.post('/reservaVehiculo',(req,res,next)=>{
    const queURL = `${URL_RESERVAS}reservaVehiculo`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( respuesta => {
        res.json({
            respuesta
        });
    });
});

app.post('/reservaHotel',(req,res,next)=>{
    const queURL = `${URL_RESERVAS}reservaHotel`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( respuesta => {
        res.json({
            respuesta
        });
    });
});

app.post('/reservaPack',(req,res,next)=>{
    const queURL = `${URL_RESERVAS}reservaConjunta`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( respuesta => {
        res.json({
            respuesta
        });
    });
});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API GW  ejecutandose en ${port}`);
});