'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');


const EXP_TIME = 24*60;
const SECRET = 'mipalabrasecreta';

function creaToken(user){
    const payload = {
        sub: user._id,
        iat:moment().unix(),
        exp:moment().add(EXP_TIME, 'minutes').unix(),
    }
    return jwt.encode(payload, SECRET);
}

function decodificaToken(token){
    return new Promise((resolve, reject)=> {
        try{
            const payload = jwt.decode(token,SECRET, true);
            if(payload.exp <= moment().unix()){
                reject({
                    status:400,
                    message: 'El token ha caducado'
                });
            }
            resolve(payload.sub);
        }catch{
            reject({
                status:500,
                message: 'El token no es válido'
            });
        }
    });
}

module.exports = {
    creaToken,
    decodificaToken
}