'use strict'

const port = process.env.PORT || 3600;

const https = require('https');
const fs = require('fs');
const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const Token = require('./services/token.service');

const app = express();

var db = mongojs('mongodb+srv://api-usr0:5340165s@cluster0.l5pdv.mongodb.net/apiHoteles?retryWrites=true&w=majority'); //conexion con la BD
var id = mongojs.ObjectID;

function auth(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
        return next(new Error("falta token de autorizacion"));
    }
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            return next();
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
            return next(err);
        });
}


//declaramos Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.get('/listaHoteles',auth,(req,res,next)=>{
    db.collection('hoteles').find((err,elementos)=> {
        if (err) return next(err); //propagamos el error
        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: 'hoteles',
            elementos: elementos
        });
    });
});

app.get('/hoteles/:id',auth,(req,res,next)=>{
    const queId = req.params.id;
    db.collection('hoteles').findOne({_id:id(queId)},(err,elementos)=> {
        if (err) return next(err); //propagamos el error
        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: 'hoteles',
            elementos: elementos
        });
    });
});

//FORMATO HOTEL
//nombre:
//nHabitacion:
//ciudad:
//fecha:
//precio:

app.post('/hoteles',auth,(req,res,next)=>{
    const nuevoElemento = req.body;
    const hotel = {
        nombre: nuevoElemento.nombre,
        nHabitacion: nuevoElemento.nHabitacion,
        ciudad: nuevoElemento.ciudad,
        fecha: nuevoElemento.fecha,
        precio:nuevoElemento.precio,
        reservado: false
    }
    db.collection('hoteles').save(hotel, (err, elementoGuardado) =>{
        if (err) return next(err); //propagamos el error
        res.status(201).json({
            result:'OK',
            colección: 'hoteles',
            elemento: elementoGuardado
        });
    });
});

app.put('/hoteles/:id', auth,(req,res,next)=>{
    const queId = req.params.id;
    const nuevosDatos = req.body;
    db.collection('hoteles').update(
        {_id:id(queId)},
        {$set: nuevosDatos},
        {safe: true, multi: false},
        (err, resultado) => {
            if (err) return next(err); //propagamos el error
            res.json({
                result: 'OK',
                coleccion: 'hoteles',
                resultado: resultado
            });
        }
    );
});

app.delete('/hoteles/:id', auth,(req,res,next)=>{
    const queId = req.params.id;
    db.collection('hoteles').remove(
        {_id:id(queId)},
        (err, resultado) => {
            if (err) return next(err); //propagamos el error

            console.log(resultado);
            res.json({
                result: 'OK',
                coleccion: 'hoteles',
                elemento: queId,
                resultado: resultado
            });
        }
    );
});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`Puerto: ${port} / api hoteles`);
});