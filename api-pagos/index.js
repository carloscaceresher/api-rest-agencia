'use strict'

const port = process.env.PORT || 3800;

const https = require('https');
const fs = require('fs');
const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const Token = require('./services/token.service');
const moment = require('moment');

const app = express();

var db = mongojs('mongodb+srv://api-usr0:5340165s@cluster0.l5pdv.mongodb.net/apiPagos?retryWrites=true&w=majority'); //conexion con la BD
var id = mongojs.ObjectID;

function auth(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
        return next(new Error("falta token de autorizacion"));
    }
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            return next();
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
            return next(err);
        });
}


//declaramos Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


app.get('/pagos',auth,(req,res,next)=>{
    db.collection('pagos').find((err,elementos)=> {
        if (err) return next(err); //propagamos el error

        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: 'pagos',
            elementos: elementos
        });
    });

});

app.get('/pagos/:id',auth,(req,res,next)=>{
    const queId = req.params.id;
    db.collection('pagos').findOne({_id:id(queId)}, (err,elementos)=> {
        if (err) return next(err); //propagamos el error

        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: 'pagos',
            elementos: elementos
        });
    });

});

//FORMATO DE PAGO
//usuario:id
//Cantidad:
//fecha:moment

app.post('/pagos',auth,(req,res,next)=>{
    const nuevoElemento = req.body;
    const pago = {
        usuario: nuevoElemento.usuario,
        cantidad: nuevoElemento.cantidad,
        fecha: moment().unix()
    }
    db.collection('pagos').save(pago, (err, elementoGuardado) =>{
        if (err) return next(err); //propagamos el error
        console.log(elementoGuardado);
        res.status(201).json({
            result:'OK',
            colección: 'pagos',
            elemento: elementoGuardado
        });
    });
});


https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`Puerto: ${port} / api pagos ejecutandose`);
});