'use strict'

const port = process.env.PORT || 3900;
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;

const URL_VUELOS = 'https://localhost:3400/';
const URL_VEHICULOS = 'https://localhost:3500/';
const URL_HOTELES = 'https://localhost:3600/';

const URL_PAGOS = 'https://localhost:3800/';
const URL_RESERVAS = 'https://localhost:3700/';

const https = require('https');
const fs = require('fs');
const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const Token = require('./services/token.service');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');

function auth(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
        return next(new Error("falta token de autorizacion"));
    }
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            return next();
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
            return next(err);
        });
}

const app = express();
//declaramos Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

//lista de vuelos disponibles
app.get('/listaVuelos',auth,(req,res,next)=>{
    const queURL = `${URL_VUELOS}listaVuelos`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( json => {
        res.json({
            result: 'OK',
            colecciones: 'vuelos',
            elementos: json.elementos
        });
    });
});

//lista de vehiculos disponibles
app.get('/listaVehiculos',auth,(req,res,next)=>{
    const queURL = `${URL_VEHICULOS}listaVehiculos`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( json => {
        res.json({
            result: 'OK',
            colecciones: 'vehiculos',
            elementos: json.elementos
        });
    });
});

//lista de hoteles disponibles
app.get('/listaHoteles',auth,(req,res,next)=>{
    const queURL = `${URL_HOTELES}listaHoteles`;
    const queToken = req.headers.authorization.split(" ")[1];
    fetch(queURL,{
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization':`Bearer ${queToken}`
        }

    })
    .then( resp => resp.json() )
    .then( json => {
        res.json({
            result: 'OK',
            colecciones: 'hoteles',
            elementos: json.elementos
        });
    });
});


app.post('/reservaVuelo',auth,(req,res,next)=>{
    const id = req.body.id;
    const queURL = `${URL_VUELOS}vuelos/${id}`;
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            fetch(queURL,{
                method:'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization':`Bearer ${queToken}`
                }
            })
            .then( resp => resp.json() )
            .then( json => {
                if(json.elementos.reservado){
                    res.status(400).json({
                        result:'KO',
                        message:'el elemento ya está reservado'
                    });
                }
                //reservamos el vuelo
                const nuevosDatos = {
                    reservado: true
                }
                fetch(queURL, {
                    method:'PUT',
                    body: JSON.stringify(nuevosDatos),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });
                //pagamos
                var pagoSatisfactorio = (Math.random()*5)==0? false : true ;
                //un fallo en el pago
                if(!pagoSatisfactorio){
                    //anulamos la reserva
                    const reseteo = {
                        reservado: false
                    }
                    fetch(queURL, {
                    method:'PUT',
                    body: JSON.stringify(reseteo),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                    });
                    res.status(400).json({
                        result:'KO',
                        message:'pago ha fallado'
                    });
                }
                //creamos el pago
                const pago = {
                    usuario: userId,
                    cantidad: json.elementos.precio
                }
                fetch(`${URL_PAGOS}pagos`, {
                    method:'POST',
                    body: JSON.stringify(pago),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });

                //creamos la reserva
                const reserva = {
                    usuario: userId,
                    productos:[{vuelo:id}],
                    precio: json.elementos.precio
                }
                fetch(`${URL_RESERVAS}reservas`, {
                    method:'POST',
                    body: JSON.stringify(reserva),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });
                //todo ha ido OK
                res.status(200).json({
                    result:'OK',
                    reserva:reserva
                });
            }).catch(err => {
                res.status(400).json({
                    result:'KO',
                    message:'no hay elementos con esa id'
                });
            });
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
        });
});

app.post('/reservaVehiculo',auth,(req,res,next)=>{
    const id = req.body.id;
    const queURL = `${URL_VEHICULOS}vehiculos/${id}`;
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            fetch(queURL,{
                method:'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization':`Bearer ${queToken}`
                }
            })
            .then( resp => resp.json() )
            .then( json => {
                if(json.elementos.reservado){
                    res.status(400).json({
                        result:'KO',
                        message:'el elemento ya está reservado'
                    });
                }
                //reservamos el vehiculo
                const nuevosDatos = {
                    reservado: true
                }
                fetch(queURL, {
                    method:'PUT',
                    body: JSON.stringify(nuevosDatos),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });
                //pagamos
                var pagoSatisfactorio = (Math.random()*5)==0? false : true ;
                //un fallo en el pago
                if(!pagoSatisfactorio){
                    //anulamos la reserva
                    const reseteo = {
                        reservado: false
                    }
                    fetch(queURL, {
                    method:'PUT',
                    body: JSON.stringify(reseteo),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                    });
                    res.status(400).json({
                        result:'KO',
                        message:'pago ha fallado'
                    });
                }
                //creamos el pago
                const pago = {
                    usuario: userId,
                    cantidad: json.elementos.precio
                }
                fetch(`${URL_PAGOS}pagos`, {
                    method:'POST',
                    body: JSON.stringify(pago),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });

                //creamos la reserva
                const reserva = {
                    usuario: userId,
                    productos:[{vehiculo:id}],
                    precio: json.elementos.precio
                }
                fetch(`${URL_RESERVAS}reservas`, {
                    method:'POST',
                    body: JSON.stringify(reserva),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });
                //todo ha ido OK
                res.status(200).json({
                    result:'OK',
                    reserva:reserva
                });
            }).catch(err => {
                res.status(400).json({
                    result:'KO',
                    message:'no hay elementos con esa id'
                });
            });
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
        });
});

app.post('/reservaHotel',auth,(req,res,next)=>{
    const id = req.body.id;
    const queURL = `${URL_HOTELES}hoteles/${id}`;
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            fetch(queURL,{
                method:'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization':`Bearer ${queToken}`
                }
            })
            .then( resp => resp.json() )
            .then( json => {
                if(json.elementos.reservado){
                    res.status(400).json({
                        result:'KO',
                        message:'el elemento ya está reservado'
                    });
                }
                //reservamos el vehiculo
                const nuevosDatos = {
                    reservado: true
                }
                fetch(queURL, {
                    method:'PUT',
                    body: JSON.stringify(nuevosDatos),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });
                //pagamos
                var pagoSatisfactorio = (Math.random()*5)==0? false : true ;
                //un fallo en el pago
                if(!pagoSatisfactorio){
                    //anulamos la reserva
                    const reseteo = {
                        reservado: false
                    }
                    fetch(queURL, {
                    method:'PUT',
                    body: JSON.stringify(reseteo),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                    });
                    res.status(400).json({
                        result:'KO',
                        message:'pago ha fallado'
                    });
                }
                //creamos el pago
                const pago = {
                    usuario: userId,
                    cantidad: json.elementos.precio
                }
                fetch(`${URL_PAGOS}pagos`, {
                    method:'POST',
                    body: JSON.stringify(pago),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });

                //creamos la reserva
                const reserva = {
                    usuario: userId,
                    productos:[{hotel:id}],
                    precio: json.elementos.precio
                }
                fetch(`${URL_RESERVAS}reservas`, {
                    method:'POST',
                    body: JSON.stringify(reserva),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                });
                //todo ha ido OK
                res.status(200).json({
                    result:'OK',
                    reserva:reserva
                });
            }).catch(err => {
                res.status(400).json({
                    result:'KO',
                    message:'no hay elementos con esa id'
                });
            });
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
        });
});

//
//{
//  vuelo:
//  hotel:
//  vehiculo:
//}
app.post('/reservaConjunta',auth,(req,res,next)=>{
    const idVuelo = req.body.vuelo;
    const idHotel = req.body.hotel;
    const idVehiculo = req.body.vehiculo;

    if(idVehiculo==null||idHotel==null||idVehiculo==null){
        res.status(400).json({
            result:'KO',
            message:'La reserva está incompleta'
        });
    }
    const queToken = req.headers.authorization.split(" ")[1];
    
    Token.decodificaToken(queToken)
    .then(userId=>{
        //vuelo
        fetch(`${URL_VUELOS}vuelos/${idVuelo}`,{
            method:'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization':`Bearer ${queToken}`
            }
        })
        .then( resp => resp.json() )
        .then( vuelo => {
            if(vuelo.elementos.reservado){
                res.status(400).json({
                    result:'KO',
                    message:'el vuelo ya está reservado'
                });
            }
            //hotel
            fetch(`${URL_HOTELES}hoteles/${idHotel}`,{
                method:'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization':`Bearer ${queToken}`
                }
            })
            .then( resp => resp.json() )
            .then( hotel => {
                if(hotel.elementos.reservado){
                    res.status(400).json({
                        result:'KO',
                        message:'el hotel ya está reservado'
                    });
                }
                //vehiculo
                fetch(`${URL_VEHICULOS}vehiculos/${idVehiculo}`,{
                    method:'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization':`Bearer ${queToken}`
                    }
                })
                .then( resp => resp.json() )
                .then( vehiculo => {
                    if(vehiculo.elementos.reservado){
                        res.status(400).json({
                            result:'KO',
                            message:'el vehiculo ya está reservado'
                        });
                    }
                    //reservamos todas las ofertas
                    const nuevosDatos = {
                        reservado:true
                    }
                    fetch(`${URL_VUELOS}vuelos/${idVuelo}`, {
                        method:'PUT',
                        body: JSON.stringify(nuevosDatos),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization':`Bearer ${queToken}`
                        }
                    });
                    fetch(`${URL_HOTELES}hoteles/${idHotel}`, {
                        method:'PUT',
                        body: JSON.stringify(nuevosDatos),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization':`Bearer ${queToken}`
                        }
                    });
                    fetch(`${URL_VEHICULOS}vehiculos/${idVehiculo}`, {
                        method:'PUT',
                        body: JSON.stringify(nuevosDatos),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization':`Bearer ${queToken}`
                        }
                    });

                    //pago
                    var cantidad = parseInt(vuelo.elementos.precio) + parseInt(hotel.elementos.precio) + parseInt(vehiculo.elementos.precio);
                    //pagamos
                    var pagoSatisfactorio = (Math.random()*5)==0? false : true ;
                    //un fallo en el pago
                    if(!pagoSatisfactorio){
                        //anulamos la reserva
                        const reseteo = {
                            reservado: false
                        }
                        fetch(`${URL_VUELOS}vuelos/${idVuelo}`, {
                            method:'PUT',
                            body: JSON.stringify(reseteo),
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization':`Bearer ${queToken}`
                            }
                        });
                        fetch(`${URL_HOTELES}hoteles/${idHotel}`, {
                            method:'PUT',
                            body: JSON.stringify(reseteo),
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization':`Bearer ${queToken}`
                            }
                        });
                        fetch(`${URL_VEHICULOS}vehiculos/${idVehiculo}`, {
                            method:'PUT',
                            body: JSON.stringify(reseteo),
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization':`Bearer ${queToken}`
                            }
                        });
                        res.status(400).json({
                            result:'KO',
                            message:'pago ha fallado'
                        });
                    }
                    //creamos el pago
                    const pago = {
                        usuario: userId,
                        cantidad: cantidad
                    }
                    fetch(`${URL_PAGOS}pagos`, {
                        method:'POST',
                        body: JSON.stringify(pago),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization':`Bearer ${queToken}`
                        }
                    });

                    //creamos la reserva
                    const reserva = {
                        usuario: userId,
                        productos:[{hotel:idHotel},{vuelo:idVuelo},{vehiculo:idVehiculo}],
                        precio: cantidad
                    }
                    fetch(`${URL_RESERVAS}reservas`, {
                        method:'POST',
                        body: JSON.stringify(reserva),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization':`Bearer ${queToken}`
                        }
                    });
                    //todo ha ido OK
                    res.status(200).json({
                        result:'OK',
                        message:'reserva completada con exito',
                        reserva: reserva
                    });
                })
                .catch(err => {
                        res.status(400).json({
                            result:'KO',
                            message:'no hay vehiculos con esa id'
                        });
                    });
                //vehiculo
            })
            .catch(err => {
                    res.status(400).json({
                        result:'KO',
                        message:'no hay hoteles con esa id'
                    });
                });
            //hotel
        })
        .catch(err => {
                res.status(400).json({
                    result:'KO',
                    message:'no hay vuelos con esa id'
                });
            });
        //vuelo
    }).catch(err => {
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
    });

});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`Puerto: ${port} / api transacciones ejecutandose`);
});
