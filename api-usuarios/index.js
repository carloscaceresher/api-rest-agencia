'use strict'

const port = process.env.PORT || 4100;

const https = require('https');
const fs = require('fs');
const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const moment = require('moment');
const Password = require('./services/pass.service');
const Token = require('./services/token.service');

const app = express();

var db = mongojs('mongodb+srv://api-usr0:5340165s@cluster0.l5pdv.mongodb.net/bbddAgencia?retryWrites=true&w=majority'); //conexion con la BD
var id = mongojs.ObjectID;
/*
function auth(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
        return next(new Error("falta token de autorizacion"));
    }
}
*/
//declaramos Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


//FORMATO USUARIOS
//_id:''
//email:''
//nickname:
//password:
//signUpDate:
//lastLoginDate:

app.post('/signup',(req,res,next)=>{
    const nuevoElemento = req.body;
    const usuario = {
        email: nuevoElemento.email,
        nickname: nuevoElemento.nickname,
        password: nuevoElemento.password,
        signUpDate: moment().unix(),
        lastLoginDate: moment().unix()
    }
    //comprobacion usuario con mismo email
    db.collection('usuarios').findOne({email:usuario.email}, (err,usuarioCreado)=>{
        if (err) return next(err);
        if(!usuarioCreado){
            Password.encriptaPassword(usuario.password)
                .then( hash => {
                usuario.password = hash;
                db.collection('usuarios').save(usuario, (err, elementoGuardado) =>{
                    if (err) return next(err); //propagamos el error
                    console.log(elementoGuardado);
                    res.status(201).json({
                        result:'OK',
                        colección: 'usuarios',
                        elemento: elementoGuardado
                    });
                });
        });
        }else{
            res.status(400).json({
                result:'KO',
                mensaje:'El email que intentas registrar ya existe'
            });
        }
    });
});

//FORMATO de logueo
//email:''
//password:''


app.post('/login',(req,res,next)=>{
    const credenciales = req.body;
    db.collection('usuarios').findOne({email:credenciales.email}, (err, usuario)=>{
        if(usuario){
            Password.comparaPassword(credenciales.password,usuario.password)
                .then(comparacion => {
                    if(comparacion){
                        //creamos token
                        const token = Token.creaToken(usuario);
                        //actualizamos la fecha de ultimo login
                        const tiempo = {
                            lastLoginDate:moment().unix()
                        } 
                        db.collection('usuarios').update(
                            {_id:id(usuario._id)},
                            {$set: tiempo},
                            {safe: true, multi: false},
                            (err, resultado) => {
                                if (err) return next(err); //propagamos el error
                                res.status(201).json({
                                    result:'OK',
                                    message:'Logueo exitoso',
                                    token: token
                                });
                            }
                        );
                    }else{
                        res.status(400).json({
                            result:'KO',
                            mensaje:'Contraseña incorrecta'
                        });
                    }
                });
        }else{ //no esta registrado
            res.status(400).json({
                result:'KO',
                mensaje:'El usuario indicado no existe'
            });
        }
    });
});


https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`Puerto: ${port} / api usuarios ejecutandose`);
});
