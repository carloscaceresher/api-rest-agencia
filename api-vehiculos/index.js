'use strict'

const port = process.env.PORT || 3500;

const https = require('https');
const fs = require('fs');
const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const Token = require('./services/token.service');

const app = express();

var db = mongojs('mongodb+srv://api-usr0:5340165s@cluster0.l5pdv.mongodb.net/apiVehiculos?retryWrites=true&w=majority'); //conexion con la BD
var id = mongojs.ObjectID;

function auth(req,res,next){
    if(!req.headers.authorization){
        res.status(401).json({
            result:'KO',
            message:'Necesaria autorización'
        });
        return next(new Error("falta token de autorizacion"));
    }
    const queToken = req.headers.authorization.split(" ")[1];
    Token.decodificaToken(queToken)
        .then(userId=>{
            return next();
        }).catch(err => {
            res.status(401).json({
                result:'KO',
                message:'Necesaria autorización'
            });
            return next(err);
        });
}


//declaramos Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.get('/listaVehiculos',auth,(req,res,next)=>{
    db.collection('vehiculos').find((err,elementos)=> {
        if (err) return next(err); //propagamos el error
        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: 'vehiculos',
            elementos: elementos
        });
    });
});

app.get('/vehiculos/:id',auth,(req,res,next)=>{
    const queId = req.params.id;
    db.collection('vehiculos').findOne({_id:id(queId)},(err,elementos)=> {
        if (err) return next(err); //propagamos el error
        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: 'vehiculos',
            elementos: elementos
        });
    });
});

//FORMATO VEHICULO
//nombre:
//origen:
//destino:
//fechaIda:
//fechaVuelta:
//precio:

app.post('/vehiculos',auth,(req,res,next)=>{
    const nuevoElemento = req.body;
    const vehiculo = {
        nombre: nuevoElemento.nombre,
        origen: nuevoElemento.origen,
        destino: nuevoElemento.destino,
        fechaIda: nuevoElemento.fechaIda,
        fechaVuelta: nuevoElemento.fechaVuelta,
        precio: nuevoElemento.precio,
        reservado: false
    }
    db.collection('vehiculos').save(vehiculo, (err, elementoGuardado) =>{
        if (err) return next(err); //propagamos el error
        res.status(201).json({
            result:'OK',
            colección: 'vehiculos',
            elemento: elementoGuardado
        });
    });
});

app.put('/vehiculos/:id', auth,(req,res,next)=>{
    const queId = req.params.id;
    const nuevosDatos = req.body;
    db.collection('vehiculos').update(
        {_id:id(queId)},
        {$set: nuevosDatos},
        {safe: true, multi: false},
        (err, resultado) => {
            if (err) return next(err); //propagamos el error
            res.json({
                result: 'OK',
                coleccion: 'vehiculos',
                resultado: resultado
            });
        }
    );
});

app.delete('/vehiculos/:id', auth,(req,res,next)=>{
    const queId = req.params.id;
    db.collection('vehiculos').remove(
        {_id:id(queId)},
        (err, resultado) => {
            if (err) return next(err); //propagamos el error

            console.log(resultado);
            res.json({
                result: 'OK',
                coleccion: 'vehiculos',
                elemento: queId,
                resultado: resultado
            });
        }
    );
});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`Puerto: ${port} / api vehiculos`);
});